# Setup & usage

There are several options to install phan. For project use, the
installation through a composer dependency seems to be the most
viable one. But for our quick evaluation, we can also just
load the current `phan.phar` file from its [release page](https://github.com/phan/phan/releases),
in this case [version 2.5.0](https://github.com/phan/phan/releases/download/2.5.0/phan.phar.asc)
and put it into our scanners collection under `scanners/phan/`.

Phan needs `php-ast` in version 1.0.1 or higher as a dependency,
but the debian repo only has php-ast in version 0.1.6
```bash
sudo apt install php-pear
sudo apt install php-dev
sudo pecl install ast-1.0.5
```
And then add `extension=ast.so` to `/etc/php/7.3/cli/php.ini`

Now phan can be run, listing available options `php ~/scanners/phan/phan.phar --help`:
[phan01.png](phan01.png)

It is also quite easy to run out-of-the box tests on single files or a directory's content:
[phan02.png](phan02.png)

For more complex setups there are many options to configure _phan_ either
via command line arguments or through a config file. The project documentation
also suggests to start with strengthening ones analysis incrementally by
starting with a relaxed analysis first and then increase the amount of
things _phan_ looks for in consecutive analysis runs:
[Incrementally Strengthening Analysis](https://github.com/phan/phan/wiki/Incrementally-Strengthening-Analysis)


# General assessment

_phan_ looks like a well developed static analyser for overall PHP code quality.
It also has plugins/extensions for the editors _vim_, _emacs_ and _VS Code_,
which could make it quite useful for integration into introductory programming education.
The plugin/extension system is well documented and one could write security-specific
plugins, although it is a lot less simple than with e.g. _graudit_, where you just
have to add simple matching rules.
In terms of security there is no specific focus and none of the issues in our
specific test data was found.


# Applied to test data

Applied to our specific test data, none of the security issues were found.
But it found a lot of general issues that are important for code quality,
and which could not have been found by a simple grepper like _graudit_.

As an example, scanning the whole folder containing the sqli related
code templates of the _DVWA_,
25 of the 26 found issues are due to use of undeclared functions or variables,
which is only the case because we scan a folder within the whole application:
[phan03.png](phan03.png)

If we scan the whole _DVWA_ folder and filter for only those issues coming from
the `vulnerabilities/sqli/` folder, the hit count is reduced to 8, with 7
still coming from undeclared variables:
[phan04.png](phan04.png)

A definite advantage of _phan_ compared to _graudit_ is that it provides
informations about what it found. This helps to look for specific issues
and provides an overall overview of the state of a whole application. Here
for example is a count of found issues for the whole DVWA (only the first screen):
[phan05.png](phan05.png)

Also _phan_ does not just do simple pattern matching but works directly
on the abstract syntax tree of the scanned PHP code. In that sense it is
a true static code analyser, whereas _graudit_ is just a pseudo analyser.


# Adaptation to SCRAP

_phan_ already provides categories and minimal explanations on found
issues. This can be used by a wrapper script. Unfortunately no security
specific issues have been found in the test data. Therefore at least
one plugin would have to be developed for _phan_ that focuses on
security issues.
