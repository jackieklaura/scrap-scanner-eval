# Setup & usage

The SonarQube Server can be installed manually from a ZIP archive,
but there is also a docker container available, which is especially
helpful for evaluative purposes. For a productive setup the manual
installation might make more sense, depending on available
hardware resources.

The docker installation is detailed here:
https://docs.docker.com/install/linux/docker-ce/debian/

After the docker setup, the SonarQube server can be started
with the following one liner:

```bash
sudo docker run -d --name sonarqube -p 9000:9000 sonarqube
```

After installation the interface is available on http://localhost:9000,
or http://scrap:9000, if the hostname is configured as in our test setup.
The credential for an initial login can be found on the
[Get Started in Two Mintes page](https://docs.sonarqube.org/latest/setup/get-started-2-minutes/)
of the SonarQube documentation.

To scan a code base we first have to create a project in SonarQube,
where we have to choose an API token, the language to analyze and
the operating system. We then also get a download link for the scanner
and some documentation on how to start a scan, as seen in
screenshot [sonarqube01.png](sonarqube01.png).

The scanner can be installed on any host that can communicate with
the SonarQube server. In our case it is the same host. While there
is also a _sonar-scanner-cli_ Docker image, to be consistent in
the test setup, I installed the scanner manually in the `~/scanners`
directory:

```bash
cd ~/scanners
wget "https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.2.0.1873-linux.zip"
unzip sonar-scanner-cli-4.2.0.1873-linux.zip
mv sonar-scanner-4.2.0.1873-linux sonar-scanner
```

A first test run with our whole vulnerability test data can be started with:
```bash
scanners/sonar-scanner/bin/sonar-scanner \
  -Dsonar.projectKey=scrap-vuln-data \
  -Dsonar.host.url=http://scrap:9000 \
  -Dsonar.login=839279eea43ef40185cc1f3dacd6d44146b852fb \
  -Dsonar.sources=./scrap-vuln-data
```

The project dashboard on the server interface then updates automatically to
highlight the results, as seen in screenshot [sonarqube02.png](sonarqube02.png).


# General assessment

SonarQube is its own mature platform for code analysis, covering many
languages and including advanced features. It is advertised not only
als general purpose code quality scanning platfrom but also as a
security focused platform.

This is highlighted especially in the newly introduced _Security Hotspot_
reviewing workflow. As [sonarqube03a.png](sonarqube03a.png) shows, the
list of found issues provides filters for many characteristics. Besides
the severity of the issues, they are also classified into code smells,
bugs and vulnerabilites, and security categories can be provided based
on the OWASP Top 10, the SANS Top 25, or available date from the CWE database.
Unfortunately no vulnerability was found in the displayed project scan,
which was a first evaluative scan of the whole `scrap-vuln-data` directory,
as explained in the setup section above.

Nevertheless some _Security Hotspots_ could be identified, as screenshot
[sonarqube03b.png](sonarqube03b.png) shows. For each hotspot, not only the
category and the relevant code sections are highlighted, but also there
are three tabs:

- "What's the risk?" as shown in [sonarqube03c.png](sonarqube03c.png)
- "Are you at risk?" as shown in [sonarqube03d.png](sonarqube03d.png)
- "How can you fix it?" as shown in [sonarqube03e.png](sonarqube03e.png)

This points in the right direction and is in line with research on
secure coding education, as highlighted in the sections on
our test data set and possible adaptation for SCRAP below.
Given the evaluation SonarQube looks like a very promising
candidate for re-evaluation at a later point in time. That is,
unless SonarQube is used at an educational organization and
integrated into programming education already, then it certainly
would make more sense to improve its vulnerability detection
than to try to integrate it into another plattform.


# Applied to test data

SonarQube is not tailored towards one-time single file analyses,
but rather to incremental analyses of a code base with a thorough
project set up. Therefore the evaluation of the test data as-is
is rather cumbersome, without initial adaptation.

To make it more feasible for the comparative analysis, a test
script has been written in `analyse.sh`, which can be used
for temporary SonarQube project instantiation an scanning
of a single files or directories, to retrieve the results
and then remove the temporary project. This is also a proof
of concept for a workflow as it could be used for SCRAP. 

The analysis results for the comparison table have then
been produced with the following commands:

```bash
scrap-eval/SonarQube/analyse.sh scrap-temp scrap-vuln-data/sqli_low.php > sqli_low
scrap-eval/SonarQube/analyse.sh scrap-temp scrap-vuln-data/sqli_medium.php > sqli_medium
scrap-eval/SonarQube/analyse.sh scrap-temp scrap-vuln-data/sqli_high.php > sqli_high
scrap-eval/SonarQube/analyse.sh scrap-temp scrap-vuln-data/xss_r_low.php > xss_r_low
scrap-eval/SonarQube/analyse.sh scrap-temp scrap-vuln-data/xss_r_medium.php > xss_r_medium
scrap-eval/SonarQube/analyse.sh scrap-temp scrap-vuln-data/xss_r_high.php > xss_r_high
scrap-eval/SonarQube/analyse.sh scrap-temp scrap-vuln-data/sqli > sqli
scrap-eval/SonarQube/analyse.sh scrap-temp scrap-vuln-data/sqli_blind > sqli_blind
scrap-eval/SonarQube/analyse.sh scrap-temp scrap-vuln-data/xss_r > xss_r
scrap-eval/SonarQube/analyse.sh scrap-temp scrap-vuln-data/xss_s > xss_s
scrap-eval/SonarQube/analyse.sh scrap-temp scrap-vuln-data/xss_d > xss_d
```

Further processing was done with using different `jq` filters to find the
number of found issues, the types, messages and full issue descriptions,
as an exemplary screenshot in [sonarqube04.png](sonarqube04.png) shows.

The issues found in these results all have been counted as unspecific hits
in the results comparison, as the SonarQube Web API does not provide any
means to access the newly featured workflow for reviewing _Security Hotspots_ (see below).

As the security hotspots where reviewed manually in the web interface for a
project scanning the whole `scrap-vuln-data` folder, some vulnerabilites
could be found, but none of those that we actually wanted to test for.
A common patter was to find e.g. a XSS vulnerability in the files featuring
an SQL injection, a DoS vulnerability in the files featuring the actual XSS,
and issues of weak cryptography in a file of the `sqli_blind` set, where
the `rand` function was used to sleep a (pseudo)random amount of seconds,
as can be seen in the exemplary screenshot [sonarqube05.png](sonarqube05.png).
Therefore, no actual hits have been entered in the comparison table,
and only those general findings from the analysis above have been counted.


# Adaptation to SCRAP

SonarQube has a usable RESTful Web API, wich is documented on the running server:
`http://scrap:9000/web_api`

Getting issues for a component (project, directory, file, ...) is therefore
quite easy, given a user token was created (alternatively user credentials
can be used with HTTP Basic authentication, but the use of a revokable token
should be the prefered method, especially if this is documented publicly as here):

```bash
curl -s -u bb3fbae1a08695d69d596debef18b12d54936cb6: scrap:9000/api/issues/search?componentKeys=scrap-vuln-data:scrap-vuln-data/sqli_low.php | jq .
```

The new workflow to review _Security Hotspots_ (as detailed above in the
general assessment section) was only introduced in the current version 8.2,
which was announced on [February 26th, 2020](https://www.sonarqube.org/sonarqube-8-2/).
The Web API did not provide any means to access the security hotspots.
The next version to be released (currently 8.3.) can be tested under
[https://next.sonarqube.com](https://next.sonarqube.com), and a look at
its [Web API](https://next.sonarqube.com/sonarqube/web_api/) shows that
there are already several endpoints that are not yet available in
version 8.2. Nevertheless also in 8.3 there is not hint towards
reviewing the security hotspots. Probably this feature will be
implemented in a future version yet to come. For the current
evaluation for SCRAP, this means that SonarQube is not as
usable as other tools already tested, despite its otherwise
intriguing web interface and the Web API.
But it has to be noted, that the feature as it is implemented
in the web interface points in the right direction and fulfills
many of the criteria that have been proposed by the research
on secure coding education, that was reviewed in my thesis.

There is also [documentation for writing own coding rules](https://docs.sonarqube.org/latest/extend/adding-coding-rules/),
which would also be necessary to improve the vulnerability
detection. But as the SonarQube project seems to progress steadily,
the most efficient thing would be to make a re-evaluation of its
coming major releases, before "duct taping" it to SCRAP.

