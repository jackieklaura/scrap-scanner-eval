#!/bin/bash

source $(dirname $0)/analyse.inc
BASEURL="http://scrap:9000"
SCANNER="${HOME}/scanners/sonar-scanner/bin/sonar-scanner"
TIMEOUT=10

## This script is for evaluation purposes only!
## Therefore no input checking is done. Make sure to use
## a project (& token) name that does not exist yet.
project=$1
src=$2

log () {
	echo "$(date): $1" 1>&2
}
logempty () {
	echo 1>&2
}

# Right after analysis, the results may not yet be available, because
# the SonarQube server is still analyzing. We can use this function later
# to check if the analysis results are available.
getAnalysisCount () {
	curl -s -u ${USERTOKEN} \
		${BASEURL}/api/project_analyses/search?project=${project} \
		| jq .paging.total
}

log "Creating project ${project}:"
curl -s -u ${USERTOKEN} \
	-F name=${project} \
	-F project=${project} \
	${BASEURL}/api/projects/create \
	1>&2
logempty

log "Creating scanning token for ${project}"
scantoken=$(curl -s -u ${USERTOKEN} \
	-F name=${project} \
	${BASEURL}/api/user_tokens/generate \
	| jq .token | tr -d '"')
log "Token: ${scantoken}"

log "Starting scanner"
${SCANNER} -Dsonar.projectKey=${project} \
	-Dsonar.host.url=${BASEURL} \
	-Dsonar.login=${scantoken} \
	-Dsonar.sources=${src} \
	1>&2


# We have to wait some time until the SonarQube server finished
# the analysis and all results are available, before we can
# actually fetch the results.
aCount=$(getAnalysisCount)
timer=0
while [ ${aCount} -eq 0 -a ${timer} -le ${TIMEOUT} ]; do
	log "Analysis not done yet. Waiting a second."
	((timer=$timer+1))
	if [ ${timer} -ge ${TIMEOUT} ]; then
		echo "Timeout reached! Aborting analysis."
		exit 1
	fi
	sleep 1
	aCount=$(getAnalysisCount)
done

log "Fetching analysis results"
curl -s -u ${USERTOKEN} \
	${BASEURL}/api/issues/search?componentKeys=${project}

log "Revoking temporary scan token ${scantoken}"
curl -s -u ${USERTOKEN} \
	-F name=${project} \
	${BASEURL}/api/user_tokens/revoke \
	1>&2

log "Removing project ${project}"
curl -s -u ${USERTOKEN} \
	-F project=${project} \
	${BASEURL}/api/projects/delete \
	| jq . \
	1>&2

