# Setup & usage

_PHPMD_ can be used directly from a `.phar` package released
by the project. It comes with different sets of rules and one
has to choose which rule set to use for the analysis, while
a mixture of different sets and custom rule set files can be chosen.

To make it usable in our scanner test array, we just download the
_phar_ package into a new `phpmd` subfolder:

```bash
mkdir ~/scanners/phpmd && cd ~/scanners/phpmd
wget https://phpmd.org/static/latest/phpmd.phar
```

In its simplest form we can call it like this, using only the _cleancode_
rule set and printing the results as a json object:

```bash
php ~/scanners/phpmd/phpmd.phar ~/scrap-vuln-data json cleancode
```

# General assessment

_PHPMD_ is a good general purpose tool for static analysis of PHP
code bases, when it comes to improving code quality. It comes with
several rule sets integrated and has good documentation for how
to use them and how to create custom rule sets based on the existing
detection functions. It does not seem to have any specific focus
on code security more than making sure that code is functional,
readable and bug-free (which is in itself already an important
aspect of good coding practice).

# Applied to test data

When we apply _PHPMD_ out-of-the-box to our test data set of
vulnerable code, we don't get any findings. Here is a screenshot
from calling _PHPMD_ on the whole test data, first with _text_
output and then with _json_ output:
[phpmd01.png](phpmd01.png)

To make sure that this is not due to a misconfiguration, I also
scanned the whole _DVWA_ code base with it and got a lot of hits.

Here is an example, filtering the json results running _PHPMD_ on the `DVWA/dvwa` folder,
first to check which files have triggered rules and how many, second
outputting only the last three findings from the second file in the
result:
[phpmd02.png](phpmd02.png)

This shows the very handy feature of providing the output in JSON format,
as well as the good annotation of the rules and their categories, as well
as where it was found.


# Adaptation to SCRAP

Based on a quick test drive of _PHPMD_ it looks like a solid framework
that could be used for its versatile output handling. Custom rule sets
could be written in order to catch vulnerable code, and the annotation
feature of the rules would fit well into a SCRAP toolchain that should
provide appropriate descriptions of the findings to the user.

Nevertheless, one would have to also develop additional detector functions
that could be used in those rule sets, as it seems that not many vulnerabilities
could be described based on the existing rules. The decision whether to
go down that road depends on whether a scanner with better out-of-the-box
results can be found and adopted for SCRAP.
