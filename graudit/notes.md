
# Setup & usage

Quite easy, and well documented in the GitHub repo:
https://github.com/wireghoul/graudit

For non-permanent use, just clone the repo and run `graudit` from the
repo's root dir. See screenshot [graudit01.png](graudit01.png).
As easy is it to run it on a file: [graudit02.png](graudit02.png).
The same works for running on a directory.

The signature/rule files are also easy to find and to clone/modify.
All out-of-the-box rule-"databases" are found in the `signatures` folder:
[graudit03.png](graudit03.png).

The signature databases are just plain text files with one regex per line,
here the example of potential sql injections:
[graudit04.png](graudit04.png)


# General assessment

_graudit_ is a simple regular-expression based grepper tool that seems
to primarily support code auditors to find code segments which need
careful analysis.

# Applied to test data

In our SQLi and XSS test data it only found the SQL injection vulnerabilities.
But it does not provide any information what type of error/vulnerability/dodgy code
it found and why (based on which rules):
[graudit05.png](graudit05.png)

A full scan of the whole DVWA source generates more findings, but they
are also quite unspecific. A quick and simple analysis with an out-of-the-box
set up is not quite feasible for our purposes.

But the tool can be easily extended with other rule databases.
By using only specific databases on specific files provides
the most value.

# Adaptation to SCRAP

For adaptation in SCRAP a wrapper script would need to scan
all individual files of the submission consecutively with each
specific rule database. This way, for every code line found by
graudit, the wrapper can produce additional information on
which ruleset is responsible for this hit. In that way also
specific SQLi, XSS and other vulnerability categories can
be attached to the found code line.
