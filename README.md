# SCRAP Scanner Evaluation

**_... finding F/LOSS scanners for software security education_**

**... :one: :zero: :one: :one: :one: :microscope: :interrobang: :heavy_check_mark: :free: :heart_eyes: :computer: :mortar_board:**

To find potential static analysis tools for PHP code, to use in the SCRAP
prototype and to answer my research question, I used to initial approaches
in finding resources:

1. follow references found in the literature review of my thesis (*1)
1. crawl the web for anything additional (primarily using DuckDuckGo as a search engine)

> Note *1: if you want to get into the field of sotware security education and
> what it says about SAST tools, you can take a look at my thesis, which will
> be published somewhere in/after May 2020 at the library of the
> [FH Technikum Wien](https://www.technikum-wien.at) and will also be
> available at https://scrap.tantemalkah.at

Based on this attempt I gathered the following four colletions of tools
for further investigation:

- AWESOME: https://github.com/mre/awesome-static-analysis
- CERN: https://security.web.cern.ch/security/recommendations/en/code_tools.shtml
- OWASP: https://owasp.org/www-community/Source_Code_Analysis_Tools - this also refers to AWESOME as an "even broader list of free static analysis tools (not just for security)"
- SAMATE: https://samate.nist.gov/index.php/Source_Code_Security_Analyzers.html - this also refers to AWESOME and WPLIST
- SWAMP: https://www.mir-swamp.org/#tools/public
- WPLIST: https://en.wikipedia.org/wiki/List_of_tools_for_static_code_analysis

From theses sources I compiled a list of free / F/LOSS static analysers that
either exlusively analyse PHP code or also provide analysis for PHP code among
others, as described in the next section.

## PHP static analysers and SAST tools

The following table summarises the tools I identified as potentially useful for
further investigation. The table lists the tool name, a link to the project
page and the tags of one or more of the collections above, on which the tool
was listed:

| Name | Link | Found on |
| ---- | ---- | -------- |
| Coverity (*1) | https://www.synopsys.com/software-integrity/security-testing/static-analysis-sast.html | WPLIST |
| Coverity Scan (*1) | https://scan.coverity.com | WPLIST |
| graudit | https://github.com/wireghoul/graudit | AWESOME |
| Parse | https://github.com/psecio/parse | AWESOME |
| phan | https://github.com/etsy/phan | AWESOME |
| PHPMD | https://phpmd.org/ | SWAMP, AWESOME |
| PHPSA | https://github.com/ovr/phpsa | AWESOME |
| PHPStan | https://github.com/phpstan/phpstan | AWESOME |
| php-sat | http://program-transformation.org/PHP/PhpSat | SAMATE |
| PHP_CodeSniffer | https://github.com/squizlabs/PHP_CodeSniffer | SWAMP, AWESOME |
| phpcs-security-audit v2 | https://github.com/FloeDesignTechnologies/phpcs-security-audit | OWASP |
| Php Inspections (EA Extended) | https://github.com/kalessil/phpinspectionsea | AWESOME |
| PHP Malware Finder (PMF) | https://github.com/nbs-system/php-malware-finder | web search |
| Progpilot | https://github.com/designsecurity/progpilot | AWESOME |
| RATS | https://code.google.com/archive/p/rough-auditing-tool-for-security/ | SAMATE, CERN |
| RIPS 0.5 | https://github.com/ripsscanner/rips | WPLIST |
| RIPS (*1) | https://www.ripstech.com | from RIPS 0.5 |
| SonarQube | https://www.sonarqube.org | WPLIST |
| WAP | http://awap.sourceforge.net/ | SAMATE, AWESOME |
| Yasca | https://www.scovetta.com/yasca | WPLIST |

> Note *1: Allthough Coverity is proprietary, it was included in this list
> due to its wide adoption in F/LOSS contexts via Coverity Scan (e.g. through
> GitHub integration). Also (the new) RIPS is proprietary but is included for
> completeness and comparison, as it is a successor to the often noted RIPS 0.5

From (manually) crawling the project sites and repositories the following table
could be generated, providing some overview of what the tools offer. The table
lists whether it provides an API, here defined as having a (documented) way to
use the tool not only as a standalone app. It also tells, if the tool is
specific to PHP-code, or if PHP is just one of the languages it analyses.
Then the table also lists whether those tools are maintained, in combination
with their last state (given by version number, and the year of the last
releas, if it was more than 6 months ago). The criterium for being counted as
"maintained" was, that there was a realease within the last 6 months of checking
tools site/repo (which happened in January 2020).

| Tool     | API | PHP-specific | state | maintained | F/LOSS |
| ----     | --- | --- | ----         | --- | --- |
| Coverity | yes | no  |              |     | no  |
| graudit (*1) | no  | no  | 2.3      | yes | yes |
| Parse    | no  | yes | 0.8 (2018)   | no  | yes |
| phan     | no  | yes | 2.5.0        | yes | yes |
| PMF (*2) | no  | yes | 0.3.5        | yes | yes |
| phpcs-security-audit v2 (*3) | no | yes | 2.0.1 | yes | yes |
| PHPSA    | no  | yes | 0.6.2 (2016) | no  | yes |
| PHPMD    | no  | yes | 2.8.1        | yes | yes |
| PHPStan  | no  | yes | 0.12.11      | yes | yes |
| PHP\_CodeSniffer | no | yes | 3.5.4  | yes | yes |
| php-sat (*4) | no  | yes | alpha (2006) | no  | yes |
| Php Inspections (EA Extended) (*5) | no | yes | 4.0.3 | yes | yes |
| Progpilot | no | yes | 0.6.0 (2019) | no  | yes |
| RATS     | no  | no  | 2.4 (2013)   | no  | yes |
| RIPS 0.5 | no  | yes | 0.55 (2015)  | no  | yes |
| RIPS     | yes | yes (*6) |          |     | no  |
| SonarQube| yes | no  | 8.1          | yes | yes |
| WAP      | no  | yes | 2.1 (2015)   | no  | yes |
| Yasca    | no  | no  | 2.2 (2010)   | no  | yes |

> Note *1: more of a pseudo static analyzer, just grepping for known patterns

> Note *2: more of a pseudo static analyzer, and focusing on malware and dodgy
> code, but could be useful due to use of YARA rules

> Note *3: is a set of security rules for _PHP\_CodeSniffer_, so it depends
> on the use of _PHP\_CodeSniffer_ and extends it with a security focus.

> Note *4: was a Google Summer of Code project, apparently never developed
> further. But notable is the description of its genesis: "The first source of
> inspiration came from my work as a assistant at the course
> 'internet programmeren' (Internet Programming) (2005,2006) at my University
> department. I noticed that a lot of students where not aware of the security
> problems involved when programming PHP for the web."
> (Source: http://program-transformation.org/PHP/PhpSatOrigin)

> Note *5: is an IDE plugin for PhpStorm/Idea

> Note *6: does now also support Java but originally comes from PHP and only did
> PHP analysis in its predecessor version RIPS 0.5

Most of these tools represent general purpose analysers focusing on code quality,
but not so much specifically on vulnerabilities and code security. In the list
above only _Parse_, _PHP\_CodeSniffer_ in combination with the _phpcs-security-audit v2_
rule set, _Progpilot_, _RIPS_ and _SonarQube_ have a stated security focus. _PMF_,
as noted, has a malware focus. _phan_ and _PHPStan_ provided a quick and easy
to use online demo, which is helpful for an initial evaluation.

In the next section I describe the closer analysis of a subset of 7 tools,
which are F/LOSS and had releases within the last 6 months, excluding
_Php Inspections (EA Extended)_, as it is an IDE plugin and the focus here
lies on tools that can be used without a lot of effort on their own and have
potential for integration in the SCRAP prototype.

## Chosen for closer inspection

Out of the list above 7 tools have been chosen for a deeper evaluation. For all
of them there is a separate folder in this repository, with a _notes.md_
file describing:

1. The setup and usage of the tool
1. A general assesssment
1. Its viablity in regard to a test data set
1. Remarks on the adaptation potential for SCRAP

All folders also include screenshots of the general usage and
its application to the test data. Some folders additionally contain
small exemplary scripts or rules to highlight the potential for its adaptation
in SCRAP.

The following list provides links to the _notes.md_ files of each of the 7
tools:

- [graudit](graudit/notes.md)
- [phan](phan/notes.md)
- [PHP_CodeSniffer](PHP_CodeSniffer/notes.md)
- [PHPMD](PHPMD/notes.md)
- [PHPStan](PHPStan/notes.md)
- [PHP Malware Finder (PMF)](PMF/notes.md)
- [SonarQube](SonarQube/notes.md)

### Test data set

As a data set to test the scanners, a subset of the vulnerability files of the
[Damn Vulnerable Web Application](http://www.dvwa.co.uk/) (short: _DVWA_) was
chosen. Of particular interested are the SQLi and XSS vulnerabilities, because
these are the most likely ones to be encountered in introductory web application
programming courses. To be able to test single PHP files as well as folders
containing a collection of PHP files and/or subfolders with PHP files, the
single vulnerability files have been placed in the root of the
vulnerability test data folder, and the DVWA folders for the different
vulnerability types (SQLi, blind SQLI, reflected XSS, stored XSS and
dom-based XSS) have been copied as a whole.

To consistently generate a folder with this test data set, the bash script
[vuln-data-copy.sh](vuln-data-copy.sh) was created and can be used to reproduce
this set from a freshly unpacked _DVWA_ download.

## Résumé

To compare the different tools, every tool was tested on every of the
single vulnerability files in the root of the test data folder, as well as
on the 5 folders containing all code for the mentioned vulnerability types.
This makes for 11 separte tests for each tool.

For every test all explicit findings of a vulnerability were counted as a
_direct hit_. If one of the vulnerabilites were found but not labeled as
vulnerability / security issue, it was counted as _vulnerabilities hit as unspecific_.
All other findings were counted as _unspecific hits_.

The results of all those tests were compiled into the [results.csv](results.csv)
table. To generate some visualisation of the findings, the Python script
[makeplots.py](makeplots.py), was created, which facilitates [pandas](https://pandas.pydata.org/)
and [matplotlib](https://matplotlib.org/) to filter the data and generate
the following 5 plots:

- [scanner_comparison_overall.png](scanner_comparison_overall.png)
- [scanner_comparison_sqli_blind.png](scanner_comparison_sqli_blind.png)
- [scanner_comparison_sqli.png](scanner_comparison_sqli.png)
- [scanner_comparison_xss_r.png](scanner_comparison_xss_r.png)
- [scanner_comparison_xss_s.png](scanner_comparison_xss_s.png)

To come to the final résumé, lets have a look at the overall comparison
directly here:

![Overall comparison of scanner tools](scanner_comparison_overall.png "Overall comparison of scanner tools")

As we can see, only _PHP\_CodeSniffer_ found specific vulnerabilities.
Here we have to note, that this is only because we use it with the
_phpcs-security-audit v2_ ruleset instead of its original own ruleset.
And while _phan_, _PHPStan_ and _SonarQube_ found significantly more
general issues, most of them are due to linting issues and the fact
that the test data is a subset of the whole _DVWA_ application, which
leads to many unintialised references.

While SonarQube is promoted also with a security focus, at least for
the PHP test data set provided here, the results are not promising.
Nevertheless its framework and web user interface seem promising and
it is built to be integrated into other workflows. Yet, for an
adaptation in context of the (time-limited) SCRAP project, the
resources needed for adoption outweigh curiosity and applicability.
As stated in the [SonarQube/notes.md](SonarQube/notes.md), a future
release of _SonarQube_ might prove more promising, as the new
workflow to review _Security Hotspots_ was only introduced in its
most recent release.

Also notable is that _graudit_ did find the SQL injection vulnerabilites,
but does not provide any information on what it acutally found.
After all, it is just a simple regular-expression based grepper tool.
On the other side, it is quite easy to adapt such a tool, by providing
additional regex patterns. In the case of _PHP Malware Finder_
this lead to the realisation that in this case we could just
facilitate _yara_ as a tool that is alread made for finding
vulnerabilities based on signatures defined in YARA rules.

For those reasons the SCRAP prototype will include two exemplary
adaptations of the F/LOSS analysis tools found in this evaluation:

- _PHP\_CodeSniffer_ with the _phpcs-security-audit v2_
- _yara_ with the _PHP Malware Finder_ ruleset, extended with own rules

To check out the results, once the protoype is finished, head
over to https://scrap.tantemalkah.at.

## License

The content of this project itself is licensed under the
[Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)
license, all source code produced to generate this content or to provide an
example is licensed under the [GNU AGPLv3](LICENSE) license.
