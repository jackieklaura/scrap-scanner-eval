global private rule IsPhp
{
  strings:
    $php = /<\?[^x]/

  condition:
    $php and filesize < 5MB
}

rule SQLi
{
  meta:
    issue = "Your code might by vulnerable to an SQL injection"
    reason = "The $id parameter seems to not be sanitized"
    info = "https://scrap/description/sqli"

  strings:
    $unsanitized = /\$id\s*=\s*\$_REQUEST\[\s*['"]id['"]\s*\]\s*;/ nocase
    $injection = /SELECT.*FROM.*WHERE.*=\s*'\$id'/ nocase

  condition:
    $unsanitized and $injection
}
