# Setup & usage

_PMF_ works by applying Yara rules, so we need the `yara` package:

```bash
sudo apt install yara
```

Then we just clone the _PMF_ repo, and to be consistent in our scanner
usage, make a link to the `php-malware-finder` in the subdirectory:

```bash
git clone https://github.com/jvoisin/php-malware-finder.git
ln -s php-malware-finder/php-malware-finder pmf
```

And now we can scan right away with:
```bash
~/scanners/pmf/phpmalwarefinder ~/scrap-vuln-data
```

# General assessment

_PHP Malware Finder_ is a good tool to scan a web host for deployed
sites that use PHP code, in order to find malware like web shells
and other dodgy or potentially obfuscated code. It relies on a
pseudo-static analysis based on a set of YARA rules and is therefore
rather easy to extend. Also the deployment is quite easy.

# Applied to test data

_PMF_ did not find any of the vulnerabilites in our test data set
and it did find only minor other issues. Even applied to the whole
vulnerable test data folder, olny 4 general issues have been found,
two coming from links to a page on _pentestmonkey.net_:
[pmf01.png](pmf01.png)

To its defence it has to be mentioned, that its intention is not to
strictly analyse code but to find known patterns for dodgy and
potentially vulnerable code, but most of all to find known malware
signatures in the analysed code base.


# Adaptation to SCRAP

To adapt _PMF_ for usage in SCRAP, not only a wrapper script
would be needed but also a whole set of YARA rules have to
be developed to find vulnerable code that it is not detected
by the provided YARA rule file in _PMF_.

A simple rule for exactly the issue in the `sql_low.php` file
in our vulnerable test data would be the following:

```yara
rule SQLi
{
  strings:
    $unsanitized = /\$id\s*=\s*\$_REQUEST\[\s*['"]id['"]\s*\]\s*;/ nocase
    $injection = /SELECT.*FROM.*WHERE.*=\s*'\$id'/ nocase

  condition:
    $unsanitized and $injection
}
```

Put into our own `scrap.yara` file we then can use _PMF_ to
find the vulnerability.

```bash
~/scanners/pmf/phpmalwarefinder -v -c scrap.yar scrap-vuln-data/sqli_low.php
```

But as _PMF_ is basically just a wrapper for _yara_, bundled with
a set of YARA rules, and as the screenshot in [pmf02.png](pmf02.png)
shows, we can achieve the same thing (even without the warning about
unbounded `.*` regexp filters) by directly calling yara:

```bash
yara -w -s scrap.yar scrap-vuln-data/sqli_low.php
```

This even entails the advantage that we can add meta information
to our yara rule, which then can be output by yara by adding the
`-m` option to the call above. Here is the annotated rule:

```yara
rule SQLi
{
  meta:
    issue = "Your code might by vulnerable to an SQL injection"
    reason = "The $id parameter seems to not be sanitized"
    info = "https://scrap/description/sqli"

  strings:
    $unsanitized = /\$id\s*=\s*\$_REQUEST\[\s*['"]id['"]\s*\]\s*;/ nocase
    $injection = /SELECT.*FROM.*WHERE.*=\s*'\$id'/ nocase

  condition:
    $unsanitized and $injection
}
```

The screenschot in [pmf03.png](pmf03.png) shows the annotated output
of `yara` with our simple rule for a SQL injection.

This all shows that it does not make sense to adapt _PMF_ directly.
If anything we could potentially use some rules out of its YARA
rule set and extend it with our own YARA rules.

