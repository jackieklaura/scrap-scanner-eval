#!/bin/bash

SOURCEDIRS="sqli sqli_blind xss_d xss_r xss_s"
SOURCESINGLES="\
	sqli/source/low.php \
	sqli/source/medium.php \
	sqli/source/high.php \
	xss_r/source/low.php \
	xss_r/source/medium.php \
	xss_r/source/high.php \
	"

usage () {
	echo "Usage: $0 <DVWA-root> <target-dir>"
	echo
	echo "	DVWA-root   ... root directory of the DVWA source"
	echo "	target-dir  ... directory to copy vulnerability test data to"
	echo "	                (target-dir should not exist)"
	echo
}

if [ "$1" = "-h" -o "$1" = "--help" -o "$1" = "help" ]; then
	usage
	exit
fi

if [ ! -d "$1" ]; then
	echo "First parameter has to be the directory containing DVWA!"
	usage
	exit 1
fi

if [ -z "$2" ]; then
	echo "Second parameter has to be the target dir for vulnerability data"
	usage
	exit 1
fi

if [ -e "$2" ]; then
	if [ -d "$2" ]; then
		if [ -z "$(ls -A "$2")" ]; then
			echo "Target dir exists and empty. All fine."
		else
			echo "The target directory already exists and has content. Aborting."
			exit 1
		fi
	else
		echo "$2 already exists and is not a directory. Aborting."
		exit 1
	fi
fi

for f in $SOURCEDIRS; do
	if [ ! -d "$1/vulnerabilities/$f" ]; then
		echo "The directory $1/vulnerabilities/$f does not exist!"
		exit 1
	fi
done

for f in $SOURCESINGLES; do
	if [ ! -f "$1/vulnerabilities/$f" ]; then
		echo "The file $1/vulnerabilities/$f does not exist!"
		exit 1
	fi
done

echo "All necessary files and directories seem to be there."
echo "Creating target folders and copying vulnerability test data."

if [ ! -d "$2" ]; then
	mkdir -p "$2"
fi

for f in $SOURCEDIRS; do
	echo "'$1/vulnerabilities/$f' -> '$2/$f'"
	cp -a "$1/vulnerabilities/$f" "$2/$f"
done

for f in $SOURCESINGLES; do
	type=$(echo $f | cut -f 1 -d '/')
	level=$(echo $f | cut -f 3 -d '/')
	cp -av "$1/vulnerabilities/$f" "$2/${type}_${level}"
done

echo "DONE. Vulnerability test data is now available at ${PWD}/$2"

