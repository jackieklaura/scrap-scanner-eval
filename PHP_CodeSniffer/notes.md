# Setup & usage

*PHP\_CodeSniffer* releases can be installed with composer, but
are also provided as phar files and therefore quite easy to set up
for an evaluative usage:

```bash
mkdir ~/scanners/phpcs && cd ~/scanners/phpcs
wget https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar
```

To print help and see get some info on the many configuration options
(also see screenshot [phpcs01.png](phpcs01.png)):

```bash
php ~/scanners/phpcs/phpcs.phar -h
```

A first out-of-the box scan can now be performed:

```bash
php ~/scanners/phpcs/phpcs.phar ~/scrap-vuln-data/sqli_low.php
```

Screenshot [phpcs02.png](phpcs02.png) shows the standard output,
which is based on applying the [PEAR Coding Standard](https://pear.php.net/manual/en/standards.php).

As there is already a set of security-specific rules for *PHP\_CodeSniffer*,
called _[phpcs-security-audit](https://github.com/FloeDesignTechnologies/phpcs-security-audit)_
we want to do our tests on this basis.

Therefore we clone the _phpcs-security-audit_ repo to our scanners directory,
and create a shorter link for brevity:

```bash
cd ~/scanners
git clone https://github.com/FloeDesignTechnologies/phpcs-security-audit.git
ln -s phpcs-security-audit phpcs-sa
```

Now we can use their _Security_ standard with the same scan as above
(as exampled also in screenshot [phps03.png](phpcs03.png)):

```bash
php ~/scanners/phpcs/phpcs.phar --standard=~/scanners/phpcs-sa/Security ~/scrap-vuln-data/sqli_low.php
```

# General assessment

*PHP\_CodeSniffer* itself is a well developed and mature framework for general
purpose code quality analysis, comparable to _PHPStan_, but with even more
configurability and different coding standards as analytical reference
to choose from, all on board. Interestingly, both projects have 179 contributors
listed on GitHub (checked on 2020-03-05), but while _PHPStan_ started in
2015, *PHP\_CodeSniffer* is already around since 2006.

The maturity of the project is also visible in good documentation, a clear
and transparent version numbering and release scheme and good extendability,
especially when it comes to creating own coding standards.

Besides its use for static analysis it also comes with an additional script
for automatic correction of coding standard violations. The best general outline
on what it is and its intent can maybe taken directly from its
[Documentation page](https://github.com/squizlabs/PHP_CodeSniffer/wiki):

> PHP\_CodeSniffer is a set of two PHP scripts; the main phpcs script that tokenizes PHP,
> JavaScript and CSS files to detect violations of a defined coding standard, and a second
> phpcbf script to automatically correct coding standard violations. PHP\_CodeSniffer is
> an essential development tool that ensures your code remains clean and consistent.
>
> A coding standard in PHP\_CodeSniffer is a collection of sniff files. Each sniff file
> checks one part of the coding standard only. Multiple coding standards can be used
> within PHP\_CodeSniffer so that the one installation can be used across multiple
> projects. The default coding standard used by PHP\_CodeSniffer is the PEAR coding standard.


# Applied to test data

When we run *PHP\_CodeSniffer* out-of-the-box without the _phpcs-security-audit_
ruleset, the results are similarly generic like with _PHPStan_ or the other
general purpose static analysers. Yet, the structure of the rulesets as
"Coding Standards" and the availability of different on-board standards
gives it a slightly better feeling for how to customize and how to trace
and explain bad code.

But when we apply the _phpcs-security-audit_ coding standard on our
test data we already get a good coverage of ours vulnerabilites, without
too much false positives or issues relating to other coding standards.

The tool does not tell which specific vulnerability is found, but
points to the exact location where the vulnerability hits the application,
as seen in screenshot [phpcs04.png](phpcs04.png), when we compare the
analysis for an SQL injection and a XSS vulnerability. But this is quite
accurate, because in many cases XSS attacks build on available SQL injection
vulnerabilites. Nevertheless, the rule set can certainly be improved to
spot code pieces where specifically a XSS attack could happen due to
unsanitized script output.


# Adaptation to SCRAP

So far (having analysed all other tools in this set, except SonarQube),
*PHP\_CodeSniffer* with the _phpcs-security-audit_ rule set seems to be
the most promising candidate for adaptation in SCRAP. Also there is good
documentation on how to write ones own coding standards, including the XML
rule sets and the PHP code for the individual Sniffs used in those rule sets.

There are also a lot of options described on the
[Advanced Usage](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Advanced-Usage)
of the documentation, which can be used to limit analysis to specific sniffs
and to filter warnings and errors based on severity. And there is also
a well documented broad range of [Reporting](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Reporting)
options.

So we can not only get information about the specific sniffs that found a
vulnerability, but also output this in JSON format, as screenshot
[phpcs05.png](phpcs05.png) shows.


