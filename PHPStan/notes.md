# Setup & usage

The most straight-forward set-up for _PHPStan_ currently is
through _composer_, as  we do not have to track all dependencies
and _composer_ itself is packaged in the Debian repository:

```bash
sudo apt install composer
mkdir ~/scanners/phpstan && cd ~/scanners/phpstan
composer require --dev phpstan/phpstan
ln -s vendor/phpstan/phpstan/phpstan
```

A first execution without any arguments already provides a
a well defined user interface, as seen in screenshots
[phpstan01.png](phpstan01.png) and [phpstan02.png](phpstan02.png).

Now we can scan a file or directory:

```bash
~/scanners/phpstan/phpstan analyse ~/scrap-vuln-data/sqli_low.php
~/scanners/phpstan/phpstan analyse ~/scrap-vuln-data/sqli/
```

While a lot of configuration and customization of scans can be
applied through a `phpstan.neon` file, one important option that
can be set directly from the command line is the rule level
(currently between 0 for loose and 9 for strict). So if we
want to catch most of the issues we would apply a level 9 or
`max` to the search:

```bash
~/scanners/phpstan/phpstan analyse --level=max ~/scrap-vuln-data/sqli_low.php
~/scanners/phpstan/phpstan analyse --level=max ~/scrap-vuln-data/sqli/
```

A comparison of the single file scan for `sql_low.php` with the default
level 0 and the max level, can be seen in screenshot [phpstan02.png](phpstan03.png).


# General assessment

The main aim of _PHPStan_ was to reduce the necessity of writing
tests, as its author wrote in 2016 in a [blog post on medium.com](https://medium.com/@ondrejmirtes/phpstan-2939cd0ad0e3). When we look at the initial checks, it becomes
clear that _PHPStan_ was made for larger PHP projects applying
current web application software engineering paradigms and
PHP practices, like e.g. applying strict typing features introduced
in PHP 7. Since this post, _PHPStan_ seems to be under constant
development, and a lot has happend. This makes it a promising framework
for static analysis of PHP code bases, allthough it aims at
general purpose quality of code analysis and does not have an
explicit focus on secure code. But the development process seems solid
and very transparent (see the [author's medium.com blog posts](https://medium.com/@ondrejmirtes)),
which would provide a goog opportunity to collaborate and extend the project
with specific vulnerability analyses.

_PHPStan_ already hase several framework-specific extensions, e.g. for Doctrine,
PHPUnit, Symfony, and a lot more. Additionally there are unofficial extensions
for e.g. Laravel, Drupal, WordPress, TYPO3, and many more.
The project also has its own [online playground at phpstan.org](https://phpstan.org/),
which helps new and interested users to test out things first.

Aside from the technical aspects of the project, it is also very interesting to look at an
[April 2018 post](https://medium.com/@ondrejmirtes/looking-for-sponsors-of-the-next-major-phpstan-release-73204cce0666) and a
[December 2018 post](https://medium.com/@ondrejmirtes/next-chapter-in-phpstan-saga-3bfd7ffdb81d)
from _PHPStan's_ author on medium.com, outlining the problem with monetization
and the available time to develop the project in such F/LOSS contexts.
This points to an issue that probably many of the other projects, and
it might be also the reason why there are a lot of security-focused scanners
out there, but many of them are not maintained anymore (see the analysis in
my master thesis).


# Applied to test data

All tests have been run with the `--level=max` option, which produced
around 50% more errors than with the default level of 0.
Nevertheless no single vulnerability was found and most of the
errors originated from references to DVWA functions and global
variables not defined in the vulnerability test data.

# Adaptation to SCRAP

From the framework point of view, _PHPStan_ could be a viable option,
if security analysis can be integrated at some point. As already highlighted
above, the CLI is well-designed, and also _json_ and other output options
are available, as seen in screenshot [phpstan04.png](phpstan04.png).

The online playground would provide SCRAP users an already available
platform to test their code pieces individually outside SCRAP,
and adopt a quality- and security-based coding practice for their
future work.

