#!/usr/bin/python3

import pandas as pd
import matplotlib.pyplot as plt

csv = pd.read_csv('results.csv', sep=';')
full = csv.iloc[:, 0:5] # we don't need the comments column

# to retain the index order as found in the CSV (which was the order of evaluation)
index = [
    'graudit',
    'phan',
    'PMF',
    'PHPMD',
    'PHPStan',
    'PHP_CodeSniffer',
    'SonarQube',
]

# columns: scanner, direct hits, hits as unspecific, unspecific
overall = full.iloc[:, [0,2,3,4]]
overall = overall.groupby(['scanner name']).sum().reindex(index)
overall.plot.bar(
    title='Comparison of overall scanner findings',
    rot=5,
    figsize=(10,7),
)
plt.yscale('log')

sqli = full[full['test data set'].str.match('^sqli$')].iloc[:, [0,2,3,4]]
sqli.set_index('scanner name', inplace=True)
sqli.plot.bar(
    title='Comparison of findings for sqli folder',
    rot=10,
    figsize=(7,6),
)

sqli_blind = full[full['test data set'].str.match('^sqli_blind$')].iloc[:, [0,2,3,4]]
sqli_blind.set_index('scanner name', inplace=True)
sqli_blind.plot.bar(
    title='Comparison of findinds for sqli_blind folder',
    rot=10,
    figsize=(7,6),
)

xss_r = full[full['test data set'].str.match('^xss_r$')].iloc[:, [0,2,3,4]]
xss_r.set_index('scanner name', inplace=True)
xss_r.plot.bar(
    title='Comparison of findings for xss_r folder',
    rot=10,
    figsize=(7,6),
)

xss_s = full[full['test data set'].str.match('^xss_s$')].iloc[:, [0,2,3,4]]
xss_s.set_index('scanner name', inplace=True)
xss_s.plot.bar(
    title='Comparison of findings for xss_s folder',
    rot=10,
    figsize=(7,6),
)

# show all plots
plt.show()
